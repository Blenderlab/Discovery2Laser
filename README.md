# Disco2Laser #

A project for migrating a 3D printer (Dagoma Discovery200) to a small Laser Cutter/engraver.

 * Surface : 200mm x 200mm
 * Laser Power : 2.5W

# Todo List

- [x] Create 3D Model
- [ ] Printing 3D Head
- [ ] Mount New head on Printer
- [ ] Mount LAser on Printer

- [ ] Find a PWM Output on Melzi
- [ ] Conenct Laser to PWM output (Direct ? Indirect ?)
- [ ] Find a way to use fans correctly

- [ ] Find good software for converting to Gcode
- [ ] Update Gcode to manage Laser output 

 